import javax.swing.*;
import java.awt.*;

public class Fenetre extends JFrame {

    private JLabel lblnom, lblprenom;
    private JButton btSupp, btModif, btAjout;
    private JTextField txtnom, txtprenom;

    public Fenetre() {

        // Création de la fenêtre
        setTitle("Etudiant en informatique");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Création des composants
        JPanel cp = (JPanel) getContentPane();
        cp.setLayout(new BorderLayout());
        JPanel pann_nomPrenom = new JPanel();
        JPanel pann_txt = new JPanel();
        JPanel pann_boutons = new JPanel();
        lblnom = new JLabel("Nom : ");
        lblprenom = new JLabel("Prenom : ");
        txtnom = new JTextField("");
        txtprenom = new JTextField("");
        btSupp = new JButton("Supprimer");
        btModif = new JButton("Modifier");
        btAjout = new JButton("Ajouter");
        // Ajout des composants à la fenêtre
        pann_nomPrenom.add(lblnom);
        pann_nomPrenom.add(lblprenom);
        pann_txt.add(txtprenom);
        pann_txt.add(txtnom);
        pann_boutons.add(btSupp);
        pann_boutons.add(btModif);
        pann_boutons.add(btAjout);
        // Affichage de la fenêtre
        JPanel panneau = (JPanel) getContentPane();
        panneau.add(pann_nomPrenom, BorderLayout.NORTH);
        panneau.add(pann_boutons, BorderLayout.SOUTH);
        panneau.add(pann_txt, BorderLayout.CENTER);
    }

    public static void main(String[] args) {
        Fenetre fenetre = new Fenetre();
        fenetre.pack();
        fenetre.setVisible(true);
    }
}